# Arquivos do Workshop na UFC

Aqui você encontra todos os fontes utilizados para os materiais de divulgação.

# Como Utilizar

Os arquivos encontram-se em Markdown. Se precisar convertê-los para HTML utilize
o [Pandoc](http://pandoc.org/).
