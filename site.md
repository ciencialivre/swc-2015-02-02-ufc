<style>
dt {
    float: left;
    clear: left;
    width: 100px;
    height: 100px;
    text-align: right;
    font-weight: bold;
    margin-top: 0;
}
dd {
    min-height: 100px;
    margin: 0 0 0 110px;
}
</style>

# Oficina: Prática em Cadernos de Pesquisas Abertos

Essa oficina objetiva  apresentar os participantes à prática de Cadernos de
Pesquisas Abertos, que tem se consolidado mundialmente como uma prática
inovadora na pesquisa científica. Para a prática serão utilizadas soluções em
Software Livre como Git e GitLab, entre outros.

          Palestra de Abertura Oficina
--------- ------------------------------------------------------------------------ --------------------------------------------------------------------------------------------------
Data      02/02 às 10:30                                                           02-03/02
Local     Sala 3 (Anfiteatro da PGMAT) -  Bloco 914  -  1o. andar (Campusdo Pici). Secretaria de Tecnologia da Informação da UFC (Antigo NPD), 2º andar, bloco 901 - Campus do Pici
Inscrição Não Precisa                                                              [Aqui] (\*)

[Aqui]: https://eventioz.com.br/e/oficina-praticas-em-cadernos-de-pesquisas-abertas/registrations/new?iframe=

(\*) Esta sendo cobrado um valor simbólico de inscrição. Caso você precise de
isenção da taxa de inscrição [entre em contato por email](mailto:raniere@ime.unicamp.br).

## Programação

### 02 de Fevereiro

10:30
:   Ciência aberta na prática: Reproducibilidade, notebooks e tudo mais

    **Resumo**:

    Uma apresentação sobre o que é reproducibilidade e como open notebooks
    podem ser usados para implementá-la, com exemplos ancorados em minha
    experiência própria, com todos os percalços e vitórias no caminho.

    Vou comentar também sobre como nós fazemos ciência aberta no nosso
    laboratório, e como é a interação com (e como convencemos)
    colaboradores.

    **Palestrante**: [Luiz Carlos Irber Júnior](http://www.luizirber.org/resume/).

    Formado em Engenharia de Computação pela UFSCar, atualmente doutorando
    em ciência da computação na Michigan State University no
    [Laboratório de Genômica, Evolução e Desenvolvimento](http://ged.msu.edu)
    (Dr. C. Titus Brown). Também
    sou instrutor do Software Carpentry a seis meses, e a uma década
    tentando fazer computação mais acessível para cientistas =]

14:00
:   Introdução ao Markdown

    Markdown é uma forma de escrever textos utilizada em vários sites e que pode
    ser utilizada para cadernos de pesquisas e rascunhos de artigos.

15:15
:   Controle de versão com Git

    Git é uma ferramenta de controle de versão local que lhe possibilita
    gerenciar as versões do seu trabalho de forma organizada,
    [ao contrário como é feito aqui](http://www.phdcomics.com/comics/archive.php?comicid=1531),
    e independente do formato de arquivo que estiver utilizando.

16:30
:   Colaboração via Git/GitLab/...

    O Git também oferece uma forma 100% segura de colaborar de forma que você
    não precisará ter medo que alguém delete um arquivo importante ou apague um
    trecho importante do texto além de outras vantagens.

18:00
:   Conversa Livre sobre Ciência Livre

    Conversa entre os organizadores **e quem mais desejar participar**
    sobre as atividades do primeiro dia, os planos para o segundo dia,
    práticas exemplares em ciência livre, ...

    Será realizado no
    [Mandir](https://www.facebook.com/CardapioLactoVegetarianoEVeganoMandir).

### 03 de Fevereiro

08:30
:   Introdução a Programação Literária

    Programação Literária é o conceito de misturar no mesmo documento o texto
    que o leitor deve ler e instruções de como figuras, tabelas, ... foram
    criadas e, portanto, podem ser recriadas pelo leitor.

    Em termos de reprodutibilidade científica, o uso de programação literária é
    desejável, se não obrigatório.

10:30
:   Cálculo Simbólico com Sympy

    Introdução ao Sympy que é uma ferramenta livre, de código aberto, para
    cálculo simbólico (o mesmo tipo realizado pelo Mathematica).

14:00
:   Criação de Gráficos

    A visualização de dados é uma peça fundamental na comunicação científica.

15:15
:   Revisão

16:30
:   Encerramento

## Realização

<img src="http://www.cienciaaberta.net/cienciaaberta/wp-content/uploads/2015/01/asm.jpg" alt="Alexandre asm" width="120" height="121" class="alignnone size-full wp-image-1135" />
:   Alexandre "asm" é um entusiasta do ideal de Conhecimento Livre,
    principalmente Software Livre e Ciência Livre. Também é um ciclista urbano e tem
    sido ativo na defesa e incentivo dos modais de transporte urbano coletivos
    (ônibus, trem, metrôs e vans) e não-motorizados (patins, skates, patinetes,
    bicicletas etc).

    É estudante de doutorado em matemática na Universidade Federal do Ceará
    (UFC), onde tem tentado aplicar o conceito de Ciência Livre no
    desenvolvimento do seu trabalho acadêmico.

<img src="http://www.cienciaaberta.net/cienciaaberta/wp-content/uploads/2015/01/vct.jpg" alt="Victor Santos" width="120" height="120" class="alignnone size-full wp-image-1134" />
:   Victor Santos (vct) é atualmente pós-doc no departamento de Física da
    Universidade Federal do Ceará. Adepto do conceito de ciência aberta e do
    software livre, tem tentado aplicá-los no desenvolvimento de ferramentas que
    auxiliem o processo da pesquisa científica.

<img src="http://www.cienciaaberta.net/cienciaaberta/wp-content/uploads/2015/01/silva.raniere.jpg" alt="Raniere Silva" width="120" height="120" class="alignnone size-full wp-image-1120" />
:   Raniere é entusiasta do movimento de cultura livre, principalmente de
    Software Livre, Educação Livre e Ciência Livre. Tem contribuído com
    a [Software Carpentry Foundation](http://software-carpentry.org/),
    a [Mozilla Foundation](http://mozilla.org/) e
    a [Open Knowledge Foundation](http://okfn.org/).

    É estudante de Matemática Aplicada e Computacional na Universidade Estadual
    de Campinas (UNICAMP).

## Apoio

<img src="http://www.cienciaaberta.net/cienciaaberta/wp-content/uploads/2015/01/ufc-300x81.png" alt="UFC" width="300" height="81" class="alignnone size-medium wp-image-1142" />
:   [Pós-graduação em Matemática da UFC](http://www.mat.ufc.br/portal/ptbr/pos-graduacao).

<img src="http://www.cienciaaberta.net/cienciaaberta/wp-content/uploads/2015/01/sti.png" alt="STI-UFC" width="102" height="41" class="alignnone size-full wp-image-1145" />
:   [Secretaria de Tecnologia da Informação da UFC](http://www.sti.ufc.br/)

<img src="http://www.cienciaaberta.net/cienciaaberta/wp-content/uploads/2015/01/okf.png" alt="Open Knowledge Foundation" width="290" height="300" class="alignnone size-full wp-image-1139" />
:   [Open Knowledge Foundation Brazil](http://br.okfn.org/)
    e o [Grupo de Ciência Aberta](http://cienciaaberta.net/) são dois grupos
    atuando no Brasil em prol do conhecimento aberto.

<img src="http://www.cienciaaberta.net/cienciaaberta/wp-content/uploads/2015/01/novatec.png" alt="Editora Novatec" width="66" height="18" class="alignnone size-full wp-image-1140" />
:   A [Editora Novatec](http://novatec.com.br/) possui vários livros sobre as
    ferramentas apresentadas na oficina.
